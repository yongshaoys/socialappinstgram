//
//  Post.swift
//  Instakilo
//
//  Created by Mark on 1/9/18.
//  Copyright © 2018 Mark. All rights reserved.
//

import Foundation
class Post {
    var postId: String
    var postDescription: String?
    var postImageUrl: String?
    var postLikeCount: String
    var postTime: String
    var publisher: PublicUser
    var isLiked: [String]?
    
    init(postId: String, postDescription: String?, postImageUrl: String?, postLikeCount: String, postTime: String, publisher: PublicUser, isLiked: [String]?) {
        self.postId = postId
        self.postDescription = postDescription
        self.postImageUrl = postImageUrl
        self.postLikeCount = postLikeCount
        self.postTime = postTime
        self.publisher = publisher
        self.isLiked = isLiked
    }
}



