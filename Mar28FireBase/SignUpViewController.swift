//
//  ViewController.swift
//  Mar28FireBase
//
//  Created by 邵勇 on 3/28/18.
//  Copyright © 2018 Yong Shao. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import FirebaseDatabase
import FirebaseStorage

class SignUpViewController: BaseViewController {
    
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var txtFieldPswd: UITextField!
    @IBOutlet weak var txtFieldUserName: UITextField!
    @IBOutlet weak var txtFieldFullName: UITextField!
    @IBOutlet weak var txtFieldAddress: UITextField!
    @IBOutlet weak var txtFieldPhone: UITextField!
    @IBOutlet weak var txtFieldBirthday: UITextField!
    
    @IBOutlet weak var userImg: UIImageView!
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    
    var originalConstriant : CGFloat = 0
    
    let imagePicker = UIImagePickerController()
    
    
    var refDatabase: DatabaseReference?
    var refStorage: StorageReference?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refDatabase = Database.database().reference()
        refStorage = Storage.storage().reference()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        
        userImg.image = UIImage(named: "Add")
        userImg.layer.cornerRadius = userImg.frame.size.height/2
        userImg.layer.borderWidth = 1.0
        userImg.layer.borderColor = UIColor.black.cgColor
        //fetchUserImage()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        userImg.isUserInteractionEnabled = true
        userImg.addGestureRecognizer(tapGestureRecognizer)
        
        originalConstriant = topConstraint.constant
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        //let tappedImage = tapGestureRecognizer.view as! UIImageView
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        //allow editing is true, so have to add function to let user trim the photo
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }

    
    @objc func keyboardWillShow(notification: Notification) {
        let info: Dictionary = notification.userInfo!
        let valueOfKeyboardSizePosition: NSValue = info[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let keyboardSize = valueOfKeyboardSizePosition.cgRectValue
        let height = keyboardSize.height
        print(height)
        let valueOfKeybaordAnimation: NSValue = info[UIKeyboardAnimationDurationUserInfoKey] as! NSValue
        var time: TimeInterval = 0
        valueOfKeybaordAnimation.getValue(&time)
        topConstraint.constant = topConstraint.constant - height
        UIView.animate(withDuration: time) { () -> Void in
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        topConstraint.constant = originalConstriant
        
        let info: Dictionary = notification.userInfo!
        let valueOfKeybaordAnimation: NSValue = info[UIKeyboardAnimationDurationUserInfoKey] as! NSValue
        var time: TimeInterval = 0
        valueOfKeybaordAnimation.getValue(&time)
        
        UIView.animate(withDuration: time) { () -> Void in
            self.view.layoutIfNeeded()
        }
        
    }
    
    
    @IBAction func btnSignup(_ sender: Any) {
        
        guard let email = txtFieldEmail.text, let pswd = txtFieldPswd.text, let userName = txtFieldUserName.text, let fullName = txtFieldFullName.text, let address = txtFieldAddress.text, let phone = txtFieldPhone.text, let bDay = txtFieldBirthday.text else {
            return
        }
        
        Auth.auth().createUser(withEmail: email, password: pswd){ (user, error) in
            
            if error == nil {
                if let fUser = user{
                    let userDict = ["UserName" : userName, "FullName" : fullName, "Email" : email, "Password" : pswd, "Address" : address, "Phone" : phone, "Birthday" : bDay , "UserID" : fUser.uid]
                    
                    self.refDatabase?.child("User").child(fUser.uid).updateChildValues(userDict)
                    self.uploadUserImage(userID: fUser.uid)
                }
                
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                
                self.navigationController?.pushViewController(controller, animated: true)
                
            } else {
                
                let errorMessage = error?.localizedDescription
                let alert = UIAlertController(title: nil, message: errorMessage, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true)
            }
        }
    }
    
    func uploadUserImage(userID: String) {
        
        if let img = userImg.image{
            let data = UIImagePNGRepresentation(img)
            let metadate = StorageMetadata()
            metadate.contentType = "image/png"
            let imageName = "UserImage/\(userID).png"
            refStorage = refStorage?.child(imageName)
            refStorage?.putData(data!, metadata: metadate, completion: { (meta,error) in
                if error == nil{
                    print(meta?.downloadURL()?.absoluteString)
                    let urlString = (meta?.downloadURL()?.absoluteString)!
                    self.refDatabase?.child("User").child(userID).updateChildValues(["ProfileImageUrl" : urlString])
                }
                
            })
        }
    }
    

    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}

extension SignUpViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
//        switch textField {
//            case txtFieldEmail: txtFieldPswd.becomeFirstResponder()
//            case txtFieldPswd: txtFieldFName.becomeFirstResponder()
//            case txtFieldFName: txtFieldLName.becomeFirstResponder()
//            case txtFieldLName: txtFieldBirthday.becomeFirstResponder()
//            case txtFieldBirthday: txtFieldPhone.becomeFirstResponder()
//            case txtFieldPhone: txtFieldAddress.becomeFirstResponder()
//            default: textField.resignFirstResponder()
//        }
        
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        if textField == txtFieldAddress || textField == txtFieldBirthday || textField == txtFieldPhone {
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        if textField == txtFieldAddress || textField == txtFieldBirthday || textField == txtFieldPhone {
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        }
    }
}


extension SignUpViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            userImg.image = pickedImage
        }
        dismiss(animated: true, completion: nil)
    }
}






