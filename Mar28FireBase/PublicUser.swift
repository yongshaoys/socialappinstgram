//
//  PublicUser.swift
//  Mar28FireBase
//
//  Created by 邵勇 on 4/3/18.
//  Copyright © 2018 Yong Shao. All rights reserved.
//

import Foundation

struct PublicUser {
    var userId: String
    var fullName : String?    
    var userName : String?
    var profileImageUrl: String?
    var following: [String]?
}


