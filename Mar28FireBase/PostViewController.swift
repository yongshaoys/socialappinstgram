//
//  PostViewController.swift
//  Mar28FireBase
//
//  Created by 邵勇 on 3/29/18.
//  Copyright © 2018 Yong Shao. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import FirebaseDatabase
import SDWebImage
import SVProgressHUD
import UserNotifications
import FirebaseMessaging

class PostViewController: BaseViewController, UNUserNotificationCenterDelegate {
    
    var refDatabase : DatabaseReference?
    var refreshControl = UIRefreshControl()
    
    var postArray = [Post]()
    
    var arrFriendId = [String]()
    
    var currentUser = CurrentUser.sharedInstance
    
    @IBOutlet weak var tblPost: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl.isEnabled = true
        refreshControl.tintColor = UIColor.red
        refreshControl.isHidden = true
        refreshControl.addTarget(self, action: #selector(refreshAction), for: .valueChanged)
        tblPost.addSubview(refreshControl)
        
        
        refDatabase = Database.database().reference()
        FirebaseHandler.sharedInstance.getCurrentUser(){() in
            print("currentUserID>>>>>>>>>>>>>>>>>>> ", self.currentUser.userId)
            Messaging.messaging().subscribe(toTopic: self.currentUser.userId)
        }
        Messaging.messaging().subscribe(toTopic: "kZZPxXSaYgfUfrcPXg3mCpunFPj1")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        displayPosts()
    }
    
    @objc func refreshAction(sender: Any) {
        postArray.removeAll()
        displayPosts()
        
    }
    
    func displayPosts (){
        
        FirebaseHandler.sharedInstance.getPostList(){(posts, error) in
            self.postArray = []
            if posts != nil {
                self.postArray = posts!
            }
    
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                self.tblPost.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}



extension PostViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblPost.dequeueReusableCell(withIdentifier: "PostTableViewCell", for: indexPath) as! PostTableViewCell
        cell.postTextlbl.text = postArray[indexPath.row].postDescription
        cell.lblLikeCount.text = postArray[indexPath.row].postLikeCount
    
        //get postImg and display it
        if let imgUrlString = self.postArray[indexPath.row].postImageUrl {
            if let url = URL(string: imgUrlString) {
                cell.postImg.sd_setImage(with: url, completed: nil)
            }
        }

        //get publisher info of every post, display publisher name and profile image
        cell.lblPublisherName.text = postArray[indexPath.row].publisher.fullName
        if let imgUrlString = postArray[indexPath.row].publisher.profileImageUrl{
            if let url = URL(string: imgUrlString ){
                cell.userImg.sd_setImage(with: url, completed: nil)
            }
        }
        
        if let currentUserFriendList = currentUser.following{
            cell.btnFollow.isSelected = currentUserFriendList.contains(postArray[indexPath.row].publisher.userId)
        }
        
        if let userListLikingThisPost = postArray[indexPath.row].isLiked{
            cell.btnLike.isSelected = userListLikingThisPost.contains(currentUser.userId)
        }
        
        if postArray[indexPath.row].publisher.userId == currentUser.userId {
            //cell.btnFollow.isHidden = true
            //cell.btnLike.isEnabled = false
        }
        
        cell.btnLike.tag = indexPath.row
        cell.btnLike.addTarget(self, action: #selector(btnLikePost), for: .touchUpInside)
        cell.btnFollow.tag = indexPath.row
        cell.btnFollow.addTarget(self, action: #selector(btnFollowPublisher), for: .touchUpInside)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapFunctionOfLblLikeCount(tap:)))
        cell.lblLikeCount.isUserInteractionEnabled = true
        cell.lblLikeCount.tag = indexPath.row
        cell.lblLikeCount.addGestureRecognizer(tapGestureRecognizer)
    
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    @objc func tapFunctionOfLblLikeCount (tap: UITapGestureRecognizer){
        
        let controller = storyboard?.instantiateViewController(withIdentifier: "FollowLikingUserViewController") as! FollowLikingUserViewController
        
        if postArray[(tap.view?.tag)!].isLiked != nil {
            controller.idArray = postArray[(tap.view?.tag)!].isLiked!
        }
        navigationController?.pushViewController(controller, animated: true)
    }
    
    
    @objc func btnFollowPublisher (sender: UIButton){

        var btnFollowIsSelected = sender.isSelected
        let indexPath = NSIndexPath(row: sender.tag, section: 0)
        let cell = tblPost.cellForRow(at: indexPath as IndexPath) as! PostTableViewCell

        btnFollowIsSelected = !btnFollowIsSelected
        cell.btnFollow.isSelected = btnFollowIsSelected

        if btnFollowIsSelected {
            refDatabase?.child("User").child(currentUser.userId).child("Following").child(postArray[indexPath.row].publisher.userId).setValue(true)
        } else {
            refDatabase?.child("User").child(currentUser.userId).child("Following").child(postArray[indexPath.row].publisher.userId).setValue(NSNull())
        }
    }
    
    
    @objc func btnLikePost (sender: UIButton){
        
        var btnLikeIsSelected = sender.isSelected
        let indexPath = NSIndexPath(row: sender.tag, section: 0)
        let cell = tblPost.cellForRow(at: indexPath as IndexPath) as! PostTableViewCell

        btnLikeIsSelected = !btnLikeIsSelected
        cell.btnLike.isSelected = btnLikeIsSelected

        let postID = postArray[indexPath.row].postId
        var likeCount = postArray[indexPath.row].postLikeCount

        if btnLikeIsSelected {
            let updatedLikeCount = Int(likeCount)! + 1
            likeCount = String(updatedLikeCount)
            postArray[indexPath.row].isLiked?.append(currentUser.userId)
            refDatabase?.child("Posts").child(postID).child("IsLiked").child(currentUser.userId).setValue("true")
        } else {
            let updatedLikeCount = Int(likeCount)! - 1
            likeCount = String(updatedLikeCount)
            postArray[indexPath.row].isLiked = postArray[indexPath.row].isLiked?.filter{$0 != currentUser.userId}
            refDatabase?.child("Posts").child(postID).child("IsLiked").child(currentUser.userId).setValue(NSNull())
        }

        postArray[indexPath.row].postLikeCount = likeCount
        cell.lblLikeCount.text = likeCount
        refDatabase?.child("Posts").child(postID).child("LikeCount").setValue(likeCount)
    }
}


//extension PostViewController: PostCellDelegate{
//
//    func likeThePost(isSelected: Bool, _ sender: PostTableViewCell) {
//
//    }
//}





