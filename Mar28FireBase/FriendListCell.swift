//
//  FriendListCell.swift
//  Mar28FireBase
//
//  Created by 邵勇 on 4/5/18.
//  Copyright © 2018 Yong Shao. All rights reserved.
//

import UIKit

class FriendListCell: UITableViewCell {
    
    @IBOutlet weak var friendImg: UIImageView!
    
    @IBOutlet weak var lblFriendName: UILabel!
    
    @IBOutlet weak var btnDelete: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        friendImg.layer.cornerRadius = friendImg.frame.size.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
