//
//  FirebaseHandler.swift
//  Mar28FireBase
//
//  Created by 邵勇 on 4/3/18.
//  Copyright © 2018 Yong Shao. All rights reserved.
//

typealias FetchCurrentUserCompletionHandler = () -> Void
typealias FetchPublicUserHandler = (PublicUser?, String?) -> ()
typealias FetchFriendHandler = ([PublicUser]?,String?) -> ()
typealias FetchPostHandler = ([Post]?, String?) -> ()

import Foundation
import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth

class FirebaseHandler : NSObject {
    
    var refDatabase : DatabaseReference?
    var currUser = CurrentUser.sharedInstance
    
    private override init(){
        
    }
    
    static let sharedInstance = FirebaseHandler()
    
    
    
    
    
    func getCurrentUser (completion: @escaping FetchCurrentUserCompletionHandler) {
        
        refDatabase = Database.database().reference()
        var followingIdArr = [String]()
        guard let currentUserId = Auth.auth().currentUser?.uid else {return}
        
        refDatabase?.child("User").child(currentUserId).observe(.value, with: {(snapshot) in
            
            guard let currentUser = snapshot.value as? Dictionary<String,Any> else {
                return
            }
    
            self.currUser.userId = currentUser["UserID"] as! String
            self.currUser.email = currentUser["Email"] as! String
            self.currUser.userName = currentUser["UserName"] as? String
            self.currUser.fullName = currentUser["FullName"] as? String
            self.currUser.password = currentUser["Password"] as? String
            self.currUser.profileImageUrl = currentUser["profileImageUrl"] as? String
            self.currUser.phoneNumber = currentUser["Phone"] as? String
            
            followingIdArr = []
            if currentUser["Following"] != nil {
                for (id, _) in currentUser["Following"] as! Dictionary<String, Any> {
                    followingIdArr.append(id)
                }
            }
            self.currUser.following = followingIdArr
            completion()
        })
    }
    
    
    func getFriendList (with currentUserID: String, completion: @escaping FetchFriendHandler){
        
        var friendArr : [PublicUser] = []
        var errorMessage: String?
        let fetchFriendGroup = DispatchGroup()
        let fetchUserGroup = DispatchGroup()
        
        fetchFriendGroup.enter()
        refDatabase?.child("User").child(currentUserID).child("Following").observeSingleEvent(of: .value) {(snapshot) in
            guard let friendDict = snapshot.value as? Dictionary<String, Any> else {
                errorMessage = "No friend found"
                completion(nil, errorMessage)
                return
            }
            
            for (id, _) in friendDict {
                fetchUserGroup.enter()
                self.fetchPublicUser(with: id){(publicUser, error) in
                    fetchUserGroup.leave()
                    guard let user = publicUser else{
                        completion(nil, errorMessage)
                        return
                    }
                   friendArr.append(user)
                }
            }
            fetchUserGroup.notify(queue: .main) {
                fetchFriendGroup.leave()
            }
        }
        fetchFriendGroup.notify(queue: .main) {
            completion(friendArr, nil)
        }
        
    }
    
    
    
    func getPostList (completion: @escaping FetchPostHandler){
        
        var postArr : [Post] = []
        //var idArrOfUserLikingPost : [String] = []
        var errorMessage: String?
        let fetchPostGroup = DispatchGroup()
        let fetchUserGroup = DispatchGroup()
        
        fetchPostGroup.enter()
        refDatabase?.child("Posts").observeSingleEvent(of: .value){(snapshot) in
            guard let postDict = snapshot.value as? Dictionary<String, Any> else{
                errorMessage = "No post found"
                completion(nil, errorMessage)
                return
            }
            
            print(postDict)
            for (postId, postValue) in postDict{
                if let post = postValue as? Dictionary<String, Any> {
                    
                    let postDescription = post["PostText"] as? String
                    let postImageUrl = post["PostImgUrl"] as? String
                    let postLikeCount = post["LikeCount"] as! String
                    let postTime = post["Timestamp"] as! String
                    let publisherID = post["UserID"] as? String
                    
                    var idArrOfUserLikingPost : [String] = []
                    if let isLiked = post["IsLiked"] as? Dictionary<String, Any> {
                        for (id, _) in isLiked {
                            idArrOfUserLikingPost.append(id)
                        }
                    }
                    
                    fetchUserGroup.enter()
                    self.fetchPublicUser(with: publisherID!){(user, error) in
                        fetchUserGroup.leave()
                        guard let publisher = user else {
                            completion(nil, "having problem getting the publisher")
                            return
                        }
                        print(idArrOfUserLikingPost)
                        let postObj = Post(postId: postId, postDescription: postDescription, postImageUrl: postImageUrl, postLikeCount: postLikeCount, postTime: postTime, publisher: publisher, isLiked: idArrOfUserLikingPost)
                        postArr.append(postObj)
                        
                        }
                    
                } else {
                    errorMessage = "could not get post"
                    print(errorMessage!)
                }
            }
            fetchUserGroup.notify(queue: .main) {
                fetchPostGroup.leave()
            }
        }
        
        fetchPostGroup.notify(queue: .main) {
            for post in postArr{
                print("postId>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", post.postId)
                print(post.isLiked)
            }
            completion(postArr, errorMessage)
        }
    }
    
    
    
    
    func fetchPublicUser(with userID: String, completion: @escaping FetchPublicUserHandler) {
        var errorMessage: String?
        
        refDatabase?.child("User").child(userID).observeSingleEvent(of: .value) { (snap) in
            if let pubUsersDict = snap.value as? [String: Any] {
                let fullname = pubUsersDict["FullName"] as! String
                let username = pubUsersDict["UserName"] as! String
                let photoUrlStr = pubUsersDict["ProfileImageUrl"] as! String
                let userid = pubUsersDict["UserID"] as! String
                
                let publicUser = PublicUser(userId: userid, fullName: fullname, userName: username, profileImageUrl: photoUrlStr, following: nil)
                completion(publicUser, errorMessage)
            } else {
                errorMessage = "Error When Parsing Public User Data \n"
                completion(nil, errorMessage)
            }
        }
    }
    
}


//    let userDict = ["UserName" : , "FullName" : , "Email" : , "Password" : , "Address" : , "Phone" : , "Birthday" :  , "UserID" : , "ProfileImageUrl"]
//
//
//

//    var userId: String!
//    var email: String!
//    var fullName: String?
//    var password: String?
//    var profileImageUrl: String?
//    var userName: String?
//    var website: String?
//    var bio: String?
//    var phoneNumber: String?
//    var gender: String?
//    var posts: [String]?
//    var following: [String]?
//    var followers: [String]?




















