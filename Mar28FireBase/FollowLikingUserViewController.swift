//
//  FollowLikingUserViewController.swift
//  Mar28FireBase
//
//  Created by 邵勇 on 4/8/18.
//  Copyright © 2018 Yong Shao. All rights reserved.
//

import UIKit
import Firebase

class FollowLikingUserViewController: UIViewController {
    
    var idArray : [String] = []
    
    var userArray : [PublicUser] = []
    
    let currentUser = CurrentUser.sharedInstance
    
    var refDatabase : DatabaseReference?

    @IBOutlet weak var tblUserIsLiking: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refDatabase = Database.database().reference()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let getLikerArrGroup = DispatchGroup()
        
        
        userArray.removeAll()
        for userId in idArray{
            getLikerArrGroup.enter()
            FirebaseHandler.sharedInstance.fetchPublicUser(with: userId, completion: {(user, error) in
                getLikerArrGroup.leave()
                if error == nil{
                    print(user!)
                    self.userArray.append(user!)
                }
            })
        }
        getLikerArrGroup.notify(queue: .main){
            print(self.userArray)
            self.tblUserIsLiking.reloadData()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}



extension FollowLikingUserViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblUserIsLiking.dequeueReusableCell(withIdentifier: "LikingPostFriendCell", for: indexPath) as! LikingPostFriendCell
        
        cell.lblUserName.text = userArray[indexPath.row].userName
        
        if let urlString = userArray[indexPath.row].profileImageUrl{
            if let url = URL(string: urlString){
                cell.userImg.sd_setImage(with: url, completed: nil)
            }
        }
        
        if let currentUserFollowing = currentUser.following{
            cell.btnFollow.isSelected = currentUserFollowing.contains(userArray[indexPath.row].userId)
        }
        
        cell.btnFollow.tag = indexPath.row
        cell.btnFollow.addTarget(self, action: #selector(btnFollowUser), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    @objc func btnFollowUser(sender: UIButton){
        var btnFollowIsSelected = sender.isSelected
        let indexPath : NSIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let cell = tblUserIsLiking.cellForRow(at: indexPath as IndexPath) as! LikingPostFriendCell
        
        btnFollowIsSelected = !btnFollowIsSelected
        cell.btnFollow.isSelected = btnFollowIsSelected
        
        if btnFollowIsSelected {
        refDatabase?.child("User").child(currentUser.userId).child("Following").child(userArray[indexPath.row].userId).setValue(true)
        } else{
        refDatabase?.child("User").child(currentUser.userId).child("Following").child(userArray[indexPath.row].userId).setValue(NSNull())
        }
    }
    
    
    
}
