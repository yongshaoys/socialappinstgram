////
////  test.swift
////  Mar28FireBase
////
////  Created by 邵勇 on 4/3/18.
////  Copyright © 2018 Yong Shao. All rights reserved.
////
//
//typealias FetchCurrentUserResultHandler = (CurrentUser, String?) -> ()
//
//import Foundation
//
//
//var currentuser = CurrentUser.sharedInstance
//
//
//func GetCurrentUserProfile()
//{
//    if let uid = Auth.auth().currentUser?.uid
//    {
//        GoogleDatahandler.SharedInstance.fetchCurrentUserData(uid: uid, completion: { (obj_currentUser, error) in
//            print(obj_currentUser.email)
//            print(obj_currentUser.fullname ?? "No Full name")
//            print(obj_currentUser.following.count)
//        })
//    }
//    else if let uid = GIDSignIn.sharedInstance().currentUser.userID
//    {
//        GoogleDatahandler.SharedInstance.fetchCurrentUserData(uid: uid, completion: { (obj_currentUser, error) in
//            print(obj_currentUser.email)
//        })
//    }
//}
//
//
//func fetchCurrentUserData(uid: String, completion: FetchCurrentUserResultHandler?) {
//    var errorMessage: String?
//    //refDatabase = Database.database().reference()
//    //guard let id = Auth.auth().currentUser?.uid else { return }
//    
//    refDatabase.child("User/\(uid)").observeSingleEvent(of: .value) { [weak self] (snapshot) in
//        guard let strongSelf = self else { return }
//        
//        if let userObj = snapshot.value as? Dictionary<String,Any?>,
//            let fullName = userObj["FullName"],
//            let email = userObj["Email"],
//            let password = userObj["Password"],
//            let phoneNumber = userObj["Phonenumber"],
//            let userimageStr = userObj["UserImage"]
//        {
//            strongSelf.currentuser.userId = uid
//            strongSelf.currentuser.fullname = fullName as? String
//            strongSelf.currentuser.email = email as! String
//            strongSelf.currentuser.UserImage = userimageStr as? String
//            strongSelf.currentuser.password = password as? String
//            strongSelf.currentuser.phoneNumber = phoneNumber as? String
//            
//            self?.refDatabase.child("User/\(uid)").observe(.value, with: { (snapshot) in
//                if let publicUserDict = snapshot.value as? [String: Any] {
//                    if let followingDict = publicUserDict["Following"] as? [String: String] {
//                        for (_, followingUserId) in followingDict {
//                            self?.currentuser.following.append(followingUserId)
//                        }
//                    }
//                    else {
//                        errorMessage = "No following users"
//                    }
//                    completion?((self?.currentuser)!, errorMessage)
//                    
//                } else {
//                    errorMessage = "Error pasing public user data"
//                }
//            })
//            
//            
//        } else {
//            errorMessage = "Error pasing user data"
//        }
//    }
//}
//
//
//
