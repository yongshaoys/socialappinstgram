//
//  UserHomeViewController.swift
//  Mar28FireBase
//
//  Created by 邵勇 on 3/28/18.
//  Copyright © 2018 Yong Shao. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import FirebaseDatabase
import FirebaseStorage
import SVProgressHUD


class UserHomeViewController: BaseViewController {
    
    @IBOutlet weak var tblUserInfo: UITableView!
    
    var ref: DatabaseReference?
    
    let currentUser = CurrentUser.sharedInstance
    
    var userArray = [Dictionary<String, Any>]()

    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        userArray = []
        printUserInfo()
    }
    
    func printUserInfo(){
        SVProgressHUD.show()
        ref?.child("User").observeSingleEvent(of: .value, with: {(snapshot) in
        //ref?.child("User").observe(.value, with: {(snapshot) in
            guard let users = snapshot.value as? Dictionary<String,Any> else {
                return
            }
            for item in users {
                print(item)
                let dict = item.value as! Dictionary<String,Any>
                if (dict["UserID"] as! String) != self.currentUser.userId {
                    self.userArray.append(dict)
                }
            }
            print(self.userArray)
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.tblUserInfo.reloadData()
            }
        })
    }
    
    @IBAction func btnSignOut(_ sender: Any) {
        do{
            try Auth.auth().signOut()
            let controller = storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
            present(controller, animated: true, completion: nil)
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension UserHomeViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userArray.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblUserInfo.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! UserInfoCell
        
        cell.lblName.text = (userArray[indexPath.row]["FullName"] as! String)
        
        if let imgUrlString = self.userArray[indexPath.row]["ProfileImageUrl"] as? String {
            if let url = URL(string: imgUrlString) {
                cell.userImage.sd_setImage(with: url, completed: nil)
            }
            
        } else {
            cell.userImage.image = nil
        }
        
        
        
        cell.btnAddFriend.tag = indexPath.row
        cell.btnAddFriend.addTarget(self, action: #selector(btnAddFriendAction), for: .touchUpInside)
        
        if let friendIdArr = currentUser.following{
            cell.btnAddFriend.isSelected = friendIdArr.contains(userArray[indexPath.row]["UserID"] as! String)
        }
  
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
  
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if userArray[indexPath.row]["UserID"] as? String == Auth.auth().currentUser?.uid{
            cell.backgroundColor = UIColor(red: 0.2, green: 0.5, blue: 0.8, alpha: 0.5)
        }
    }
    
    
    
    @objc func btnAddFriendAction (sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        let userIdFollowing = userArray[sender.tag]["UserID"] as! String
        
        if sender.isSelected {
            ref?.child("User").child(currentUser.userId).child("Following").child(userIdFollowing).setValue(true)
        } else {
           ref?.child("User").child(currentUser.userId).child("Following").child(userIdFollowing).setValue(NSNull())
        }
    }
    
}


//    func fetchUserImage() {
//        if let userid = Auth.auth().currentUser?.uid{
//            let imageName = "UserImage/\(userid).png"
//            refStorage = refStorage?.child(imageName)
//            refStorage?.getData(maxSize: 1024*1024, completion: {(data, error) in
//                DispatchQueue.main.async {
//                   // self.userImg.image = UIImage(data: data!)
//                }
//            })
//        }
//    }



//    private func destroyToLogin() {
//        guard let window = UIApplication.shared.keyWindow else {
//            return
//        }
//
//        guard let rootViewController = window.rootViewController else {
//            return
//        }
//
//        // unsubscribe user from push notification
//        Messaging.messaging().unsubscribe(fromTopic: CurrentUser.sharedInstance.userId)
//
//        // Reset Current User
//        CurrentUser.sharedInstance.dispose()
//
//        let authStoryboard = UIStoryboard(name: "Authentication", bundle: Bundle.main)
//        let vc = authStoryboard.instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController
//
//        vc.view.frame = rootViewController.view.frame
//        vc.view.layoutIfNeeded()
//
//        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
//            window.rootViewController = vc
//        }, completion: { completed in
//            rootViewController.dismiss(animated: true, completion: nil)
//        })
//    }

