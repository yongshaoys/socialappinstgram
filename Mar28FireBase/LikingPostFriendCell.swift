//
//  LikingPostFriendCell.swift
//  Mar28FireBase
//
//  Created by 邵勇 on 4/8/18.
//  Copyright © 2018 Yong Shao. All rights reserved.
//

import UIKit

class LikingPostFriendCell: UITableViewCell {
    
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnFollow: UIButton!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        userImg.layer.cornerRadius = userImg.frame.size.width/2
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
