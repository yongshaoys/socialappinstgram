//
//  AddPostViewController.swift
//  Mar28FireBase
//
//  Created by 邵勇 on 3/29/18.
//  Copyright © 2018 Yong Shao. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import FirebaseDatabase
import FirebaseStorage

class AddPostViewController: BaseViewController {
    
    @IBOutlet weak var addNewImg: UIImageView!
    @IBOutlet weak var txtFieldAddNewText: UITextField!
    
    var refDataBase : DatabaseReference?
    var refStorage : StorageReference?
    
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refDataBase = Database.database().reference()
        //refStorage = Storage.storage().reference()
        
        imagePicker.delegate = self
        //imagePicker.allowsEditing = true
        print("test add post")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if addNewImg.image == nil {
            choosePhotoLibraryOrCamera()
        }
        
    }

    
    
    @IBAction func btnSubmitPost(_ sender: Any) {
        addPostToDatabase()
        addNewImg.image = nil
        if let tabVC = self.storyboard?.instantiateViewController(withIdentifier: "TabViewController") as? TabViewController {
            self.present(tabVC, animated: true, completion: nil)
        }
    }
    
    
    func addPostToDatabase (){
        
        guard let postText = txtFieldAddNewText.text else{return}
        
        if let userid = Auth.auth().currentUser?.uid {
            
            let timestamp = String(Date().timeIntervalSince1970)
            //        let myTimeInterval = TimeInterval(timestamp)
            //        let time = NSDate(timeIntervalSince1970: TimeInterval(myTimeInterval))
            
            refDataBase?.child("Posts")
            let key = refDataBase?.child("Posts").childByAutoId().key
            let post = ["PostID" : key!, "UserID" : userid, "PostText" : postText, "Timestamp" : timestamp, "LikeCount" : "0"]
            refDataBase?.child("Posts").child(key!).updateChildValues(post)
            
            uploadImage(postKey: key!)
        }
    }
    
    func uploadImage(postKey: String)  {
        if let img = addNewImg.image{
            let data = UIImagePNGRepresentation(img)
            let metadate = StorageMetadata()
            metadate.contentType = "image/png"
            let imageName = "PostImage/\(postKey).png"
            
            //no duplicate initilizaton. change it
            refStorage = Storage.storage().reference().child(imageName)
            refStorage?.putData(data!, metadata: metadate, completion: { (meta,error) in
                if error == nil{
                    print(meta?.downloadURL()?.absoluteString)
                    let urlString = (meta?.downloadURL()?.absoluteString)!
                    self.refDataBase?.child("Posts").child(postKey).updateChildValues(["PostImgUrl" : urlString])
                }
            })
        }
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension AddPostViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

extension AddPostViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            addNewImg.image = pickedImage
        }
        dismiss(animated: true, completion: nil)
    }
}

extension AddPostViewController {
    
    func choosePhotoLibraryOrCamera(){
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            
            if let tabVC = self.storyboard?.instantiateViewController(withIdentifier: "TabViewController") as? TabViewController {
                self.present(tabVC, animated: true, completion: nil)
            }
            
//            self.dismiss(animated: true) {
//                self.tabBarController?.selectedIndex = 1
//            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { _ in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        //allow editing is true, so have to add function to let user trim the photo
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
}



//    func choosePhotoLibraryOrCamera(){
//        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary){
//            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
//self.present(imagePicker, animated: true, completion: nil)
//        } else if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
//            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
//self.present(imagePicker, animated: true, completion: nil)
//        }
//    }


