//
//  CurrentUser.swift
//  Mar28FireBase
//
//  Created by 邵勇 on 4/2/18.
//  Copyright © 2018 Yong Shao. All rights reserved.
//

import Foundation
import UIKit

class CurrentUser: NSObject {
    struct Static {
        static var instance: CurrentUser?
    }
    
    //when log out, set the sharedInstance to nil
    class var sharedInstance: CurrentUser {
        if Static.instance == nil
        {
            Static.instance = CurrentUser()
        }
        
        return Static.instance!
    }
    
    func dispose() {
        CurrentUser.Static.instance = nil
        print("Disposed Singleton instance")
    }
    
    private override init() {}
    
    var userId: String!
    var email: String!
    var fullName: String?
    var password: String?
    var profileImageUrl: String?
    var userName: String?
    var website: String?
    var bio: String?
    var phoneNumber: String?
    var gender: String?
    var posts: [String]?
    var following: [String]?
    var followers: [String]?
}
