//
//  FriendViewController.swift
//  Mar28FireBase
//
//  Created by 邵勇 on 4/3/18.
//  Copyright © 2018 Yong Shao. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth

class FriendViewController: UIViewController {
    
    @IBOutlet weak var tblFriend: UITableView!
    
    var friendArr = [PublicUser]()
    var refDatabase : DatabaseReference?
    var currentUser = CurrentUser.sharedInstance

    override func viewDidLoad() {
        super.viewDidLoad()
        refDatabase = Database.database().reference()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        FirebaseHandler.sharedInstance.getFriendList(with: CurrentUser.sharedInstance.userId){(friends, error) in
            self.friendArr = []
            if friends != nil {
                self.friendArr = friends!
            }
            DispatchQueue.main.async {
                self.tblFriend.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}




extension FriendViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friendArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblFriend.dequeueReusableCell(withIdentifier: "FriendListCell", for: indexPath) as! FriendListCell
        
        cell.lblFriendName.text = friendArr[indexPath.row].fullName
        if let imgUrlString = friendArr[indexPath.row].profileImageUrl{
            if let url = URL(string: imgUrlString){
                cell.friendImg.sd_setImage(with: url, completed: nil)
            }
        }
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(btnDeleteFollow), for: .touchUpInside)
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    @objc func btnDeleteFollow (sender: UIButton){
    refDatabase?.child("User").child(currentUser.userId).child("Following").child(friendArr[sender.tag].userId).setValue(NSNull())
        
        friendArr.remove(at: sender.tag)
        
        tblFriend.reloadData()
        
        
    }
    
}
