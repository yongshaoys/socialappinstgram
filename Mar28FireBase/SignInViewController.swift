//
//  SignInViewController.swift
//  Mar28FireBase
//
//  Created by 邵勇 on 3/28/18.
//  Copyright © 2018 Yong Shao. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import GoogleSignIn
import FirebaseDatabase

class SignInViewController: BaseViewController, GIDSignInUIDelegate {
    
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var txtFieldPswd: UITextField!
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var imgInstagram: UIImageView!
    
    var refDatabase : DatabaseReference?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refDatabase = Database.database().reference()
        imgView.image = UIImage(named: "aaa")
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
    
    }
    
    @IBAction func btnGoogleSignIn(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
        
    }
    
    @IBAction func btnSignin(_ sender: Any) {
        
        Auth.auth().signIn(withEmail: txtFieldEmail.text!, password: txtFieldPswd.text!){(user,error) in
            print(user?.uid ?? "")
            
            if error != nil {
                let alertMessage = error?.localizedDescription
                let alert = UIAlertController(title: nil, message: alertMessage, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel))
                self.present(alert, animated: true)
                return
            } else {
//                if let tabController = self.storyboard?.instantiateViewController(withIdentifier: "TabViewController") as? TabViewController {
//                    self.present(tabController, animated: true, completion: nil)
//                }
                self.toLogin()
            }
   
        }
  
    }
    
    private func toLogin() {
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        
        guard let rootViewController = window.rootViewController else {
            return
        }
        
        // unsubscribe user from push notification
        // Messaging.messaging().unsubscribe(fromTopic: CurrentUser.sharedInstance.userId)
        
        // Reset Current User
        // CurrentUser.sharedInstance.dispose()
        
        let authStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = authStoryboard.instantiateViewController(withIdentifier: "TabViewController") as! TabViewController
        
        vc.view.frame = rootViewController.view.frame
        vc.view.layoutIfNeeded()
        
        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
            window.rootViewController = vc
        }, completion: { completed in
            rootViewController.dismiss(animated: true, completion: nil)
        })
    }
    
    
    @IBAction func btnSignUp(_ sender: Any) {
        let controller = storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        navigationController?.pushViewController(controller, animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension SignInViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtFieldEmail {
            txtFieldPswd.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
}

extension SignInViewController: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        
        if let error = error {
            print(error.localizedDescription)
            return
        }
  
        guard let email = user.profile.email, let pswd = user.authentication.idToken, let fullName = user.profile.name, let UserName = user.profile.givenName, let url = user.profile.imageURL(withDimension: 100) else {
            return
        }
        
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        
        Auth.auth().signIn(with: credential) { (user, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            } else {
                
                if let userId = Auth.auth().currentUser?.uid{
                    let userDict = ["FullName" : fullName, "UserName" : UserName, "UserID" : userId, "Email" : email, "Password" : pswd, "ProfileImageUrl" : url.absoluteString] as [String : Any]
                    self.refDatabase?.child("User").child(userId).updateChildValues(userDict)

                }
                
                DispatchQueue.main.async {
                    if let tabController = self.storyboard?.instantiateViewController(withIdentifier: "TabViewController") as? TabViewController {
                        self.present(tabController, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }

    
}






