//
//  PostTableViewCell.swift
//  Mar28FireBase
//
//  Created by 邵勇 on 3/29/18.
//  Copyright © 2018 Yong Shao. All rights reserved.
//

import UIKit

//protocol PostCellDelegate : class {
//    func likeThePost (isSelected : Bool, _ sender: PostTableViewCell)
//}


class PostTableViewCell: UITableViewCell {

    //weak var delegate : PostCellDelegate?
    
    @IBOutlet weak var postImg: UIImageView!
    @IBOutlet weak var userImg: UIImageView!
    
    @IBOutlet weak var postTextlbl: UILabel!
    @IBOutlet weak var lblLikeCount: UILabel!
    
    @IBOutlet weak var lblPublisherName: UILabel!
    
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    

    
    /*
     @IBAction func btnHeartAction(_ sender: UIButton) {
        delegate?.likeThePost(isSelected: sender.isSelected, self)
     }
 */

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    func tapFunctionOfLblLikeCount (){
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
